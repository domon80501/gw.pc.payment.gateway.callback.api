﻿using GW.PC.Core;
using GW.PC.Payment.Gateway.Callback.Context;
using GW.PC.Services.Queues.EF;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Payment.Gateway.Callback.Api.Controllers
{
    public abstract class PaymentCallbackGatewayController : ApiController
    {
        protected Dictionary<string, string> httpHeaders = new Dictionary<string, string>();
        protected PaymentCallbackGatewayContext PaymentCallbackGatewayContext;
        protected CallbackHandlerContext CallbackHandlerContext;

        protected async Task<HttpResponseMessage> Callback()
        {
            PaymentCallbackGatewayContext.Guid = Guid.NewGuid().ToString();

            BuildLogHeader();

            try
            {

                CallbackHandlerContext = await CallbackImp();

                LogCallbackReceived();

                if (CallbackHandlerContext.AddQueue)
                {
                    await SaveRequest();
                }

                LogCallbackResponded();

                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(CallbackHandlerContext.SuccessText, Encoding.UTF8, "text/plain")
                };
            }
            catch (Exception ex)
            {
                LogHelper.LogDepositCallbackError(PaymentCallbackGatewayContext.LogHeader, ex.ToString());

                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent("Internal callback error")
                };
            }
        }

        private void BuildLogHeader()
        {
            PaymentCallbackGatewayContext.LogHeader = $"{PaymentCallbackGatewayContext.Guid}|PaymentChannelId:{PaymentCallbackGatewayContext.PaymentChannelId}|{PaymentCallbackGatewayContext.PaymentTypeEnum}|{PaymentCallbackGatewayContext.PaymentMethodEnum}|{PaymentCallbackGatewayContext.ExtensionRouteEnum}|{PaymentCallbackGatewayContext.CallbackSuccessTypeEnum}";
        }

        private void LogCallbackReceived()
        {
            foreach (var item in Request.Headers)
            {
                httpHeaders.Add(item.Key, string.Join(";", item.Value));
            }
            if (httpHeaders != null)
            {
                CallbackHandlerContext.PaymentCallbackRequest.Headers = JsonConvert.SerializeObject(httpHeaders);
            }

            var request = CallbackHandlerContext.PaymentCallbackRequest;

            LogHelper.LogReadPaymentCallbackHeader(PaymentCallbackGatewayContext.LogHeader, request.Headers);

            if (request.PaymentType == PaymentType.Deposit)
            {
                LogHelper.LogDepositCallbackReceived(PaymentCallbackGatewayContext.LogHeader, request.Content);
            }
            else
            {
                LogHelper.LogWithdrawCallbackReceived(PaymentCallbackGatewayContext.LogHeader, request.Content);
            }
        }
        private void LogCallbackResponded()
        {
            var content = JsonConvert.SerializeObject(CallbackHandlerContext.SuccessText);
            if (CallbackHandlerContext.PaymentCallbackRequest.PaymentType == PaymentType.Deposit)
            {
                LogHelper.LogDepositCallbackResponded(PaymentCallbackGatewayContext.LogHeader, content);
            }
            else
            {
                LogHelper.LogWithdrawCallbackResponded(PaymentCallbackGatewayContext.LogHeader, content);
            }
        }

        private async Task SaveRequest()
            => await new PaymentCallbackRequestService().Add(CallbackHandlerContext.PaymentCallbackRequest);

        protected abstract Task<CallbackHandlerContext> CallbackImp();
    }
}