﻿using GW.PC.Core;
using GW.PC.Payment.Gateway.Callback.Context;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Payment.Gateway.Callback.Api.Controllers
{
    [RoutePrefix(WebApiControllerRoutePrefixes.WithdrawCallbackGateway)]
    public class WithdrawalCallbackGatewayController : PaymentCallbackGatewayController
    {
        [HttpGet]
        [HttpPost]
        [Route("{paymentchannelid}/{paymenttype}/{paymentmethod}/{extensionroute}/{successtype}/{param?}")]
        public async Task<HttpResponseMessage> Callback(string paymentChannelId, string paymentType, string paymentMethod, string extensionRoute, string successType)
        {
            PaymentCallbackGatewayContext = new PaymentCallbackGatewayContext
            {
                PaymentChannelId = paymentChannelId,
                PaymentType = paymentType,
                PaymentMethod = paymentMethod,
                ExtensionRoute = extensionRoute,
                SuccessType = successType
            };

            return await Callback();
        }

        protected override async Task<CallbackHandlerContext> CallbackImp()
        {
            return await CallbackHandlers[PaymentCallbackGatewayContext.ExtensionRouteEnum].Invoke(Request, PaymentCallbackGatewayContext);
        }

        private Dictionary<ExtensionRoute, Func<HttpRequestMessage, PaymentCallbackGatewayContext, Task<CallbackHandlerContext>>> CallbackHandlers
        {
            get
            {
                return new Dictionary<ExtensionRoute, Func<HttpRequestMessage, PaymentCallbackGatewayContext, Task<CallbackHandlerContext>>>()
                {
                    {ExtensionRoute.Default,
                        async (HttpRequestMessage httpRequest
                        , PaymentCallbackGatewayContext context )
                        =>{ return  await new CallbackHandler().Callback(httpRequest,context);}},
                };
            }
        }
    }
}