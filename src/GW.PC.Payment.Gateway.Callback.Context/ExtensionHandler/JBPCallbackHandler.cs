﻿using System.Linq;
using System.Threading.Tasks;
using GW.PC.Models.Queues;
using Newtonsoft.Json;
using System.Web;
using GW.PC.Core;

namespace GW.PC.Payment.Gateway.Callback.Context.ExtensionHandler
{
    public class JBPCallbackHandler : CallbackHandler
    {
        private string GetProviderType()
        {
            return HttpRequest.RequestUri.Segments.Last().Replace("/", "");
        }

        protected async override Task BuildFinalResult()
        {
            await base.BuildFinalResult();

            var type = GetProviderType();


            if (type.ToLower() == "addtransfer")
            {
                var data = PaymentCallbackRequest.Content.Split('&').Where(x => x.Contains("=")).Select(array => array.Split('=')).ToDictionary(x => x[0], x => x[1]);

                if (data["bank_id"] == "51" && data["deposit_mode"] == "2")
                {
                    PaymentCallbackRequest.PaymentType = PaymentType.Deposit;
                    PaymentCallbackRequest.PaymentMethod = PaymentMethod.QuickPay;
                }
            }
            else if (type.ToLower() == "withdrawalresult")
            {
                PaymentCallbackRequest.PaymentType = PaymentType.Withdrawal;
                PaymentCallbackRequest.PaymentMethod = PaymentMethod.OnlineBanking;
            }
            else
            {
                //支付商提款订单二次确认通知，不必加入queue，SuccessText 回覆指定格式即可
                addQueue = false;
            }
        }

        protected override Task<string> BuildSuccessText()
        {
            var type = GetProviderType();

            if (type != "addtransfer" && type != "withdrawalresult")
            {
                var items = HttpUtility.ParseQueryString(PaymentCallbackRequest.Content);

                return Task.FromResult(JsonConvert.SerializeObject(new
                {
                    exception_order_num = items["exception_order_num"],
                    status = "1",
                    error_msg = ""
                }));
            }
            else
            {
                var data = PaymentCallbackRequest.Content.Split('&').Where(x => x.Contains("=")).Select(array => array.Split('=')).ToDictionary(x => x[0], x => x[1]);

                if (type == "addtransfer")
                {
                    return Task.FromResult(JsonConvert.SerializeObject(new
                    {
                        company_order_num = data["company_order_num"],
                        mownecum_order_num = data["mownecum_order_num"],
                        status = "1",
                        error_msg = ""
                    }));
                }
                else
                {
                    return Task.FromResult(JsonConvert.SerializeObject(new
                    {
                        company_order_num = data["company_order_num"],
                        mownecum_order_num = data["mownecum_order_num"],
                        status = "1"
                    }));
                }
            }
        }
    }
}
