﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GW.PC.Models.Queues;
using GW.PC.Core;

namespace GW.PC.Payment.Gateway.Callback.Context.ExtensionHandler
{
    public class CSFCallbackHandler : CallbackHandler
    {
        protected override async Task<string> BuildContent()
        {
            var sign = HttpRequest.Headers.GetValues("Content-Hmac").FirstOrDefault();
            var content = Encoding.UTF8.GetString(await HttpRequest.Content.ReadAsByteArrayAsync());

            return $"{content},csf,{sign}";
        }

        protected async override Task BuildFinalResult()
        {
            await base.BuildFinalResult();

            dynamic providerData = Newtonsoft.Json.Linq.JObject.Parse(PaymentCallbackRequest.Content);

            if (providerData.direction == "in")
            {//Deposit type
                if (providerData.customer_bankflag == "UNIPAY")
                {
                    PaymentCallbackRequest.PaymentType = PaymentType.Deposit;
                    PaymentCallbackRequest.PaymentMethod = PaymentMethod.QuickPay;
                }
            }

            //Withdrawal type
            PaymentCallbackRequest.PaymentType = PaymentType.Withdrawal;
            PaymentCallbackRequest.PaymentMethod = PaymentMethod.OnlineBanking;
        }

        protected override Task<string> BuildSuccessText() => Task.FromResult("true");
    }
}
