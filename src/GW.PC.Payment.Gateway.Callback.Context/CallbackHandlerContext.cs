﻿using GW.PC.Models.Queues;

namespace GW.PC.Payment.Gateway.Callback.Context
{
    public class CallbackHandlerContext
    {
        public PaymentCallbackRequest PaymentCallbackRequest { get; set; }
        public string SuccessText { get; set; }
        public bool AddQueue { get; set; }
    }
}
