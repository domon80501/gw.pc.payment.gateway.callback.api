﻿namespace GW.PC.Payment.Gateway.Callback.Context.Enums
{
    public enum CallbackSuccessType
    {
        success = 1,
        SUCCESS = 2,
        Success = 3,
        ok = 4,
        OK = 5,
        Ok = 6,
    }
}
