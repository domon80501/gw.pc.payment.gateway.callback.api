﻿namespace GW.PC.Payment.Gateway.Callback.Context.Enums
{
    public enum ExtensionRoute
    {
        Default = 1,
        JBP = 2,
        CSF = 3,
    }
}
