﻿namespace GW.PC.Payment.Gateway.Callback.Context.Enums
{
    public enum PaymentType
    {
        Deposit = 1,
        Withdrawal,
    }
}
