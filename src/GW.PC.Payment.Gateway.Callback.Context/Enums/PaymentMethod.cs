﻿namespace GW.PC.Payment.Gateway.Callback.Context.Enums
{
    public enum PaymentMethod
    {
        WeChat = 1,
        Alipay = 2,
        OnlinePayment = 3,
        OnlineBanking = 4,
        PrepaidCard = 5,
        QQWallet = 6,
        QuickPay = 7,
        OnlineToBankCard = 8,
        JDWallet = 9,
        Internal = 101
    }
}