﻿using GW.PC.Core;
using GW.PC.Models.Queues;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Payment.Gateway.Callback.Context
{
    public class CallbackHandler
    {
        protected bool addQueue = true;
        protected Encoding encoding = Encoding.UTF8;
        protected HttpRequestMessage HttpRequest;
        protected PaymentCallbackGatewayContext PaymentCallbackGatewayContext;
        protected PaymentCallbackRequest PaymentCallbackRequest = new PaymentCallbackRequest();

        public async Task<CallbackHandlerContext> Callback(HttpRequestMessage request, PaymentCallbackGatewayContext context)
        {
            HttpRequest = request;
            PaymentCallbackGatewayContext = context;

            PaymentCallbackRequest.Content = await BuildContent();

            await BuildFinalResult();

            var successText = await BuildSuccessText();

            return new CallbackHandlerContext
            {
                PaymentCallbackRequest = PaymentCallbackRequest,
                SuccessText = successText,
                AddQueue = addQueue
            };
        }

        protected virtual async Task<string> BuildContent() => HttpRequest.Method == HttpMethod.Get ?
                 HttpRequest.RequestUri.AbsoluteUri : encoding.GetString(await HttpRequest.Content.ReadAsByteArrayAsync());

        protected virtual Task BuildFinalResult()
        {
            PaymentCallbackRequest.RequestId = PaymentCallbackGatewayContext.Guid;
            PaymentCallbackRequest.PaymentChannelId = Convert.ToInt32(PaymentCallbackGatewayContext.PaymentChannelId);
            PaymentCallbackRequest.PaymentType = PaymentCallbackGatewayContext.PaymentTypeEnum;
            PaymentCallbackRequest.PaymentMethod = PaymentCallbackGatewayContext.PaymentMethodEnum;
            PaymentCallbackRequest.CreatedAt = DateTime.Now.ToE8();
            PaymentCallbackRequest.HttpMethod = HttpRequest.Method.Method;
            PaymentCallbackRequest.Url = HttpRequest.RequestUri.AbsoluteUri;

            return Task.CompletedTask;
        }

        protected virtual Task<string> BuildSuccessText() => Task.FromResult(Convert.ToString(PaymentCallbackGatewayContext.CallbackSuccessTypeEnum));
    }
}
