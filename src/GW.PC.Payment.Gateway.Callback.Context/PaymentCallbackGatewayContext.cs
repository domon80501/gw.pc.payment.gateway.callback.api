﻿using GW.PC.Core;
using System;

namespace GW.PC.Payment.Gateway.Callback.Context
{
    public class PaymentCallbackGatewayContext
    {
        public string Guid { get; set; }
        public string LogHeader { get; set; }
        public string PaymentChannelId { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethod { get; set; }
        public string ExtensionRoute { get; set; }
        public string SuccessType { get; set; }

        public PaymentType PaymentTypeEnum => (PaymentType)Enum.Parse(typeof(PaymentType), PaymentType);
        public PaymentMethod PaymentMethodEnum => (PaymentMethod)Enum.Parse(typeof(PaymentMethod), PaymentMethod);
        public ExtensionRoute ExtensionRouteEnum => (ExtensionRoute)Enum.Parse(typeof(ExtensionRoute), ExtensionRoute);
        public CallbackSuccessType CallbackSuccessTypeEnum => (CallbackSuccessType)Enum.Parse(typeof(CallbackSuccessType), SuccessType);
    }
}
